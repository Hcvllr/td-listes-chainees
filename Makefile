CC=gcc
CFLAGS=-c Wall

all: exec

exec: main.o functions.o
	$(CC) main.o functions.o -o exec

main.o: main.c
	$(CC) $(CFLAGS) main.c

functions.o: functions.c
	$(CC) $(CFLAGS) functions.c

clean:
	rm -rf *o exec
// Listes doublement chainées contenant des élements

struct element_ {
    struct element_ * prev;
    struct element_ * next;
    int value;
};

typedef struct element_* element;

struct list_ {
    element first;
    element last;
};

typedef struct list_* list;



#include <stdio.h>
#include <stdlib.h>
#include "struct.h"

element createElement(int value)
{
    element newElement = (element) malloc( sizeof(struct element_));
    
    newElement->prev = NULL;
    newElement->next = NULL;
    newElement->value = value;
    
    return newElement;
}

list createList(int value)
{
    list newList = (list) malloc( sizeof(struct list_));
    
    newList->first = createElement(value);
    newList->last = newList->first;
    
    return newList;
}

int sizeList(list myList)
{
    int index = 0;
    element elm = myList->first;
    
    for(index = 0; elm->next != NULL; index++)
    {
        elm = elm->next;
    }
    return index+1;
}

void listAppend(list myList, int value)
{
    element newElement = createElement(value);
    newElement->prev = myList->last;
    myList->last->next =  newElement;
    myList->last = newElement;
}

void listAppendAtPos(list myList, int position, int value)
{
    if(position < sizeList(myList))
    {
        element elm = myList->first;
        element newElement = createElement(value);
        for(int i = 0; i<position; i++)
        {
            elm = elm->next;
        }
        newElement->prev = elm;
        newElement->next = elm->next;
        elm->next = newElement;  
    }
}

void listRemoveElement(list myList, int position)
{
    if(position < sizeList(myList))
    {
        element elm = myList->first;
        
        for(int i = 0; i<position; i++)
        {
            elm = elm->next;
        }

        if(elm == myList->first)
        {
            myList->first = elm->next;
        }

        else if(elm == myList->last)
        {
            myList->last = elm->prev;
        }
        
        //make the link between the previous and next element
        elm->prev->next = elm->next; 
        elm->next->prev = elm->prev;
        //free the memory
        free(elm);
    }
}

int getListValue(list myList, int position)
{
    if(position < sizeList(myList))
    {
        element elm = myList->first;
        
        for(int i = 0; i<position; i++)
        {
            elm = elm->next;
        }
        return elm->value;
    }
    else
    {
        return -1;
    }
}

void putListValue(list myList, int position, int value)
{
    if(position < sizeList(myList))
    {
        element elm = myList->first;
        
        for(int i = 0; i<position; i++)
        {
            elm = elm->next;
        }
        elm->value = value;
    }
}

void printList(list myList)
{
    element elm = myList->first;
    
    for(int index = 0; elm != NULL; index++ )
    {
        printf("L'element %3d vaut %3d\n",index,elm->value);
        elm = elm->next;
    }
}

int countInList(list myList, int value)
{
    int count = 0;
    element elm = myList->first;
    while(elm != NULL)
    {
        if(elm->value == value)
        {
            count++;
        }
        elm = elm->next;
    }
    return count;
}

void removeMatchValue(list myList, int value, int all)
{
    int finded = 0;
    element elm = myList->first;
    
    for(int pos = 0; !finded && elm!=NULL; pos++)
    {
        if(elm->value == value)
        {
            listRemoveElement(myList,pos);
            finded = 1;
        }
        elm = elm->next;
    }
    if(finded && all)
    {
        removeMatchValue(myList,value,all);
    } 
}

void fusionList(list list_A, list list_B)
{
    list_A->last->next = list_B->first;
    list_A->last = list_B->last;
    free(list_B);
}

void freeList(list myList)
{
    element elm = myList->first;
    
    while(elm != myList->last)
    {
            elm = elm->next;
            free(elm->prev);
    }
    //at this moment, the list contain only 1 element
    free(elm);
    free(myList);
}


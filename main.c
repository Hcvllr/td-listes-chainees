#include <stdio.h>
#include <stdlib.h>
#include "functions.h"
#include "struct.h"
int main(){
    
    list liste_A = createList(5);
    listAppend(liste_A, 8);
    listAppend(liste_A, 3);
    listAppend(liste_A, 4);
    listAppend(liste_A, 4);
    
    list liste_B = createList(5);
    listAppend(liste_B, 5);
    listAppend(liste_B, 12);
    listAppend(liste_B, 42);
    
   
    printf("Liste A : \n");
    printList(liste_A);
    printf("Liste B : \n");
    printList(liste_B);
    
    fusionList(liste_A,liste_B);
    
    printf("Liste A : \n");
    printList(liste_A);
    
    freeList(liste_A);

    return 0;
}

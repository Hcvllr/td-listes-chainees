typedef struct element_* element;

typedef struct list_* list;

//function return a new element(take the init value in input)
element createElement(int value);

//function return a new list contain 1 element (take the init value in input)
list createList(int value);

//return the number of elements in list
int sizeList(list myList);

//Append a new element at the end of a list (take the value of element in input)
void listAppend(list myList, int value);


//Append a new element after the specified position in list (take value and position)
void listAppendAtPos(list myList, int position, int value);

//Remove an element of a list
void listRemoveElement(list myList, int position);

//return the value of an element in list  (WARNING: return -1 if the position don't exist)
int getListValue(list myList, int position);

//modify value of an element in list
void putListValue(list myList, int position, int value);

//print the values of the elements in list
void printList(list myList);

//return the occurences of a value in a list 
int countInList(list myList, int value);

//remove the first(all=0) or all(all=1) the occurence of value in list 
void removeMatchValue(list myList, int value, int all);

//fusion 2 list (append list_2 to list_1)
void fusionList(list list_1, list list_2);

//delete and free list
void freeList(list myList);

